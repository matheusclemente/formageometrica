#include <iostream>
#include "formageometrica.hpp"
#include "circulo.hpp"
#include "quadrado.hpp"
#include "triangulo.hpp"


using namespace std;

int main(int argc, char ** argv) {

   cout << "----------------------------------------------------"<< endl;
   FormaGeometrica * forma_1 = new FormaGeometrica();
   FormaGeometrica * forma_2 = new FormaGeometrica(4, 15.0, 15.0);

//   forma_1->calculaArea();
//   forma_1->calculaPerimetro();
//   forma_2->calculaArea();
//   forma_2->calculaPerimetro();

   cout << endl;
   cout << "Forma 1 - Area: " << forma_1->getArea() << "\t Perimetro: " << forma_1->getPerimetro() << endl;
   cout << "Forma 2 - Area: " << forma_2->getArea() << "\t Perimetro: " << forma_2->getPerimetro() << endl;
   cout << "----------------------------------------------------"<< endl;


   Quadrado * forma_3 = new Quadrado(25);
   Triangulo * forma_4 = new Triangulo(12, 18);
   Circulo * forma_5 = new Circulo(5);

   forma_3->calculaArea();
   forma_3->calculaPerimetro();
   forma_4->calculaArea();
   forma_4->calculaPerimetro();
   forma_5->calculaArea();
   forma_5->calculaPerimetro();

   cout << endl;
   cout << "Forma 3 - Area: " << forma_3->getArea() << "\t Perimetro: " << forma_3->getPerimetro() << endl;
   cout << "Forma 4 - Area: " << forma_4->getArea() << "\t Perimetro: " << forma_4->getPerimetro() << endl;
   cout << "Forma 5 - Area: " << forma_5->getArea() << "\t Perimetro: " << forma_5->getPerimetro() << endl;
   cout << "----------------------------------------------------"<< endl << endl;
   //*/

   FormaGeometrica * formas[10] = {/*forma_1, forma_2,*/ forma_3, forma_4, forma_5, new Triangulo(40, 25)};
   //FormaGeometrica * formas[10] = {new Circulo(5), new Triangulo(3,4), new Quadrado(2)};

   for (int i = 0; i < 4; i++) {
		formas[i]->calculaArea();
		formas[i]->calculaPerimetro();
		cout << "A forma de " << formas[i]->getLados();
		cout << " lados tem área = " << formas[i]->getArea();
		cout << " e perímetro = " << formas[i]->getPerimetro();
		cout << endl;
	}
   //*/

   return 0;
}
