#include <iostream>
#include "circulo.hpp"

#define PI 3.1415926536f

using namespace std;

Circulo::Circulo(){
    setBase(10);
    setLados(0);
}

Circulo::Circulo(float base){
//  cout << "#Contrutor do Circulo" << endl;
    setBase(base);
    setLados(0);
}

// Circulo::~Circulo(){
//   cout << "Destrutor: Circulo" << endl;
// }

void Circulo::calculaArea(){
  //std::cout << "**Calculo de Area do Circulo" << std::endl;
    setArea((PI) * getBase() * getBase());
}

void Circulo::calculaPerimetro(){
//  std::cout << "**Calculo de Perímetro do Circulo"  << std::endl;  
    setPerimetro(2 * (PI) * getBase());
}
