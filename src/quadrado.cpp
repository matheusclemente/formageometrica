#include <iostream>
#include "quadrado.hpp"

using namespace std;

Quadrado::Quadrado(){
    setBase(20);
    setAltura(20);
}

Quadrado::Quadrado(float base){
  // cout << "#Contrutor do Quadrado" << endl;
    setBase(base);
    setAltura(base);
}

// Quadrado::~Quadrado(){
//   cout << "Destrutor: Quadrado" << endl;
// }
