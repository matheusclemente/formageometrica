#include <iostream>
#include "retangulo.hpp"

using namespace std;

Retangulo::Retangulo(float base, float altura){
  setBase(base);
  setAltura(altura);
  setLados(4);
  setArea(base*altura);
  setPerimetro(2*base + 2*altura);
}
